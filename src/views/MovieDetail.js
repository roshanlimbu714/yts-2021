import Cover from "../containers/Cover";
import {useParams} from "react-router";

const MovieDetails =(props)=>{

    const {id} = useParams();

    return (
        <section>
            <Cover title={'Movie Details '+id}/>
        </section>
    )
}

export default MovieDetails;
