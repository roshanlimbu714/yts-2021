import Cover from "../containers/Cover";
import MoviesList from "../containers/MoviesList";

const Landing =()=>{
    return (
        <section>
            <Cover title={'Now Playing'}/>
            <MoviesList/>
        </section>
    )
}

export default Landing;
