import Cover from "../containers/Cover";

const Search =()=>{
    return (
        <section>
            <Cover title={'Search Movies'}/>
        </section>
    )
}

export default Search;
