import Cover from "../containers/Cover";

const Featured =()=>{
    return (
        <section>
            <Cover title={'Featured Movies'}/>
        </section>
    )
}

export default Featured;
