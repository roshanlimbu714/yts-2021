import React from 'react';
import TopBar from "../components/common/TopBar";
import MovieDetail from "../views/MovieDetail";
import Landing from "../views/Landing";
import Search from "../views/Search";
import {Route, Switch} from "react-router";
import Featured from "../views/Featured";

const MainLayout = () => {
    return (
        <>
          <TopBar/>
          <main>
            <Switch>
                <Route path={'/featured'} component={Featured} exact={true}/>
                <Route path={'/search'} component={Search} exact={true}/>
                <Route path={'/movie-detail/:id'} component={MovieDetail} exact={true}/>
                <Route path={''} component={Landing} exact={true}/>
            </Switch>
          </main>
        </>
    )
}
export default MainLayout;
