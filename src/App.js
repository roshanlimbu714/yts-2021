import './App.scss';
import MainLayout from "./layouts/MainLayout";
import World from "./components/World";
function App() {
    return (
        <>
            <MainLayout/>
        </>
    );
}

export default App;
