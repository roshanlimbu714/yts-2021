import React, {Component} from 'react';

const headerStyle = {
    backgroundColor: 'red',
    color: 'white'
}

class Hello extends Component {
    state = {
        isDisplayed: true,
        fruits: ['apple', 'banana', 'citrus', 'guava', 'pineapple']
    }
    changeNumber = () => {
        this.setState({
            fruit: 'banana'
        });
    }
    toggleContent = () => {
        this.setState({
            isDisplayed: !this.state.isDisplayed,
            fruits: []
        })
    }
    number = 7;

    render() {
        const number2 = 10;
        return (
            <>
                {this.state.isDisplayed === true &&
                this.state.fruits.map((val, key) => (
                    <h1 key={key}>{val}</h1>
                ))
                }
                <button onClick={this.toggleContent}>Click Me</button>
            </>
        )
    }
}

export default Hello;
