import {useEffect, useState} from "react";

const World = ()=>{
    const [price,setPrice] = useState(400);
    const [discount,setDiscount] = useState(0);
    const [total, setTotal] = useState(price);
    const [appearText, setAppearText ] = useState('not appeared');
    const [count, setCount] = useState(0);

    useEffect(()=>{
    },[]);

    useEffect(()=>{
        let discountAmount = discount*price /100;
        setTotal(price- discountAmount)
    },[discount]);

    const increaseCount=()=>{
        setDiscount(discount+5);
    }

    const fruits = ['apple','banana','orange'];
    return (
        <div className="world title-xl">
            <div>
                Price: {price}
            </div>
            <div>Discount %: {discount}</div>
            <div>Total:{total}</div>
            <button className={`btn ${discount%2 ===0 ? 'primary':'secondary'} title-md`} onClick={increaseCount}> Add </button>
        </div>
    )
}

export default World;
