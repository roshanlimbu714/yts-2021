import othello from '../../assets/img/othello.jpg';

const MovieCard = ()=>{

    return (
        <div className="movie-card mb-lg">
            <div className="movie-img">
                <img src={othello} alt=""/>
            </div>
            <div className="title-xs bold movie-title">Movie Name</div>
            <div className="title-xs movie-time">1hr 3min</div>
            <div className="movie-genre">Comedy</div>
        </div>
    );
}

export default MovieCard;
