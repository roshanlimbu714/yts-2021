import {Link} from "react-router-dom";

const TopBar = ()=>{

    return (
        <nav className={'flex justify-between items-center ' +
        'wrapper-x py-md'}>
            <div className="logo">Logo</div>
            <div className="nav-items flex">
                <Link to="/" className="nav-item px-md">Home</Link>
                <Link to='/search' className="nav-item px-md">Search</Link>
                <Link to='/featured' className="nav-item px-md">Featured</Link>
            </div>
        </nav>
    );
}

export default TopBar;
