import MovieCard from "../components/common/MovieCard";
import {Link} from "react-router-dom";

const MovieList = ()=>{
    const movieIds = [1,2,3,4,5,6,7,8,9];
    return (
       <div className={'wrapper'}>
           <div className="title-md ">
               List of movies
           </div>
           <div className="movie-list mt-xl flex wrap">
               {movieIds.map( v=>
               <Link to={'/movie-detail/'+v}>
                   <MovieCard/>
               </Link>)}
           </div>
       </div>
    );
}

export default MovieList;
