const Cover = (props)=>{

    return (
      <section className="cover-section full-vh wrapper-x flex column justify-center">
          <div className={'title relative text-light'}>{props.title ?? '' }</div>
            <div className={'relative'}>
                <button className={'btn primary sub-title'}>View More</button>
            </div>
      </section>
    );
}

export default Cover;
